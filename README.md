# Front End Test Izabela Lima

To run the project you must have:

### Dependencies

```
node.js
ruby
gem sass
```

What things you need to install the software and how to install them

### Installing

```
$ npm install
```

To run:

```
$ npm run build
```

## Author

* **Izabela Moreira Germer de Lima**  [izamglima](https://izamglima.github.io)
