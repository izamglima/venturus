$('#formUsers').submit(function(event){
  var user = {address: {}, days: []};
  $("#formUsers :input").each(function(){
    switch ($(this).attr('name')){
      case "username":
        user.username = $(this).val();
        break;
      case "name":
        user.name = $(this).val();
        break;
      case "email":
        user.email = $(this).val();
        break;
      case "city":
        user.address.city = $(this).val();
        break;
      case "ride":
        if($(this).is(":checked"))
          user.ride = $(this).val();
        break;
      case "days":
        if($(this).is(":checked"))
          user.days.push($(this).val());
        break;
      default:
    }
  });
  $("#formUsers :input[type=text], [type=email]").val("");
  users.push(user);
  clearTable();
  displayTable(users);
  alert("Novo usuário criado com sucesso!");
  event.preventDefault();
});
