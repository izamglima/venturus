function displaySearchResults() {
  var search = $('#search').val();

  var options = {
    // Each element in array is an object, not a string. We can pass an `extract`
    // function that is called on each element in the array to extract the string
    // to fuzzy search against.
    extract: function(element) {
        return element.username + ' ' + element.name + ' ' + element.email + ' ' + element.address.city  ;
      }
  };

  // Filter!
  var filtered = fuzzy.filter(search, users, options);
  clearTable();
  displayTable(filtered.map(function(item) { return item.original; }));

}

$(function() {
  // Filter the characters on each change of the input
  $('#search').keyup(displaySearchResults);
});
