var users, posts, albums, photos = [];
var rides = ['Always', 'Sometimes', 'Never'];
var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
var daysGroups = {
  'Every day': days,
  'Week day': ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'],
  'Weekends': ['Sun', 'Sat']
};

function randRides() {
  return rides[Math.floor((Math.random() * 2) + 0)];
}

function randDays(){
  result = Math.floor((Math.random() * 10) + 0);
  switch (result) {
    case 0:
      return daysGroups['Every day'];
    case 1:
      return daysGroups['Week day'];
    case 2:
      return daysGroups.Weekends;
    default:
      return days[result-3];
  }
}

function displayDays(days) {
  if (daysAreEqual(days, daysGroups['Every day'])) {
    return 'Every Day';
  } else if (daysAreEqual(days, daysGroups['Week day'])) {
    return 'Week Day';
  } else if (daysAreEqual(days, daysGroups.Weekends)) {
    return 'Weekends';
  }

  return days;
}

function daysAreEqual(days, days_b) {
  if (days.length != days_b.length) {
    return false;
  }

  var areEqual = true;
  for (var i = 0; i < days.length; i++) {
    if (!days_b.includes(days[i])) {
      areEqual = false;
      break;
    }
  }

  return areEqual;
}

function countPosts(posts, user) {
  return posts.filter(function(post){
    return user.id === post.userId;
  }).length;
}

function countAlbums(albums, user) {
  return userAlbums(albums, user).length;
}

function userAlbums(albums, user) {
  return albums.filter(function(album){
    return user.id === album.userId;
  });
}

function countPhotos(photos, albums) {
  return photos.filter(function(photo){
    return albums.find(function(album) {
      return album.id === photo.albumId;
    });
  }).length;
}

//get posts
$.get( "https://jsonplaceholder.typicode.com/posts", function( data ) {
  posts = data;

  //get albums
  $.get( "https://jsonplaceholder.typicode.com/albums", function( data ) {
    albums = data;

    //get photos
    $.get( "https://jsonplaceholder.typicode.com/photos", function( data ) {
      photos = data;

      // get users
      $.get( "https://jsonplaceholder.typicode.com/users", function( data ) {
        users = data;
        displayTable(users);
      });
    });
  });
});

function mapsLink(user) {
  if (user.id) {
    return '<a target="_blank" href="https://maps.google.com/?q=' + user.address.city  + '">' +
    user.address.city + '</a>';
  }

  return user.address.city;
}

function displayTable(users){
  for (var i = 0; i < users.length; i++) {
    if (!users[i].days) {
      users[i].days = randDays();
    }
    if (!users[i].ride) {
      users[i].ride = randRides();
    }
    $('tbody').append('<tr><td class="userName">' + users[i].username + '</td><td>' +
      users[i].name  + '</td><td><a href="mailto:'+ users[i].email + ' ">' + users[i].email +
      '</a></td><td>' + mapsLink(users[i]) + '</td><td>' + users[i].ride +
      '</td><td>' + displayDays(users[i].days) + '</td><td>' + countPosts(posts, users[i]) +
      '</td><td>' + countAlbums(albums, users[i]) + '</td><td>' + countPhotos(photos, userAlbums(albums, users[i])) +
      '<td><span class="trashy"><i class="fas fa-trash-alt"></i></span></td>' +'</td></tr>');
  }
}

function clearTable(){
  $('tbody tr').remove();
}

$("body").on('click', '.trashy', function(el){
  $(this).closest('tr').remove();
});
